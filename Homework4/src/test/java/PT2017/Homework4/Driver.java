package PT2017.Homework4;









import java.util.Date;

import org.junit.Test;

import PT2017.Homework4.Account.Account;
import PT2017.Homework4.Account.SavingAccount;
import PT2017.Homework4.Bank.Bank;
import PT2017.Homework4.Person.Person;
import junit.framework.TestCase;

public class Driver extends TestCase {
	@Test
	public void test() {
		try{
			Bank bank = new Bank();
			Person person = new Person(12000,"Tache Farfuridi");
			bank.addPerson(person);
			addAccount(bank,person);
			removePersonTest(bank,person);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}
	@Test
	public void removePersonTest(Bank bank, Person person){
		bank.removePerson(person);
	}
	@Test
	public void addAccount(Bank bank, Person person){
		try {
			bank.addAccount(person,new SavingAccount(new Date(), 10000, 0, 0, person));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
