package PT2017.Homework4.fileIO;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import PT2017.Homework4.Bank.Bank;

public class ReadSequentialFile {
	
	private ObjectInputStream input;
	
	public void openFile(String path){
		try{
			input = new ObjectInputStream(Files.newInputStream(Paths.get(path)));
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public Bank readBank() throws Exception{
		
		try{
			return (Bank) input.readObject();
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception();
			
		}
	}
	
	public void closeFile(){
		try{
			if(input != null)
				input.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}
