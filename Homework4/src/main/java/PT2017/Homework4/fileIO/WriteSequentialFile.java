package PT2017.Homework4.fileIO;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import PT2017.Homework4.Bank.Bank;

public class WriteSequentialFile {
	private ObjectOutputStream output;
	
	public void openExistingFile(String path){
		try{
			output = new ObjectOutputStream(Files.newOutputStream(Paths.get(path)));
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void openNewFile(String path){
		try{
			@SuppressWarnings("unused")
			File file = new File(path);
			output = new ObjectOutputStream(Files.newOutputStream(Paths.get(path)));
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void writeFile(Bank bank){
		try {
			output.writeObject(bank);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void closeFile(){
		try{
			if(output != null)
				output.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}
