package PT2017.Homework4.View;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class AccountView {
	public Pane pane;
	public Scene scene;
	public Label l1;
	public Label l2;
	public TextField t1;
	public Button b1;
	public Button b2;
	public Button b3;
	
	
	public AccountView(){
		pane = new Pane();
		scene = new Scene(pane,300,250);
		
		l1 = new Label();
		l1.setText("Enter amount:");
		l1.setLayoutX(50);
		l1.setLayoutY(50);
		pane.getChildren().add(l1);
		
		l2 = new Label();
		l2.setText("");
		l2.setLayoutX(50);
		l2.setLayoutY(230);
		pane.getChildren().add(l2);
		
		t1 = new TextField();
		t1.setLayoutX(50);
		t1.setLayoutY(80);
		t1.setPrefSize(200, 20);
		pane.getChildren().add(t1);
		
		b1 = new Button();
		b1.setText("Deposit");
		b1.setLayoutX(50);
		b1.setLayoutY(115);
		b1.setPrefSize(95, 20);
		pane.getChildren().add(b1);
	    
		b2 = new Button();
		b2.setText("Withdraw");
		b2.setLayoutX(155);
		b2.setLayoutY(115);
		b2.setPrefSize(95, 20);
		pane.getChildren().add(b2);
	    
		b3 = new Button();
		b3.setText("Back");
		b3.setLayoutX(50);
		b3.setLayoutY(200);
		b3.setPrefSize(200, 20);
		pane.getChildren().add(b3);
	    
		
	}

}
