package PT2017.Homework4.View;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class ManageAccountView {
	public Pane pane;
	public Scene scene;
	public Label l1;
	public TextField t1;
	public Label l2;
	public TextField t2;
	public Label l3;
	public TextField t3;
	public Button b1;
	public Button b2;
	public Button b3;
	
	
	public ManageAccountView(){
		pane = new Pane();
		scene = new Scene(pane,300,350);
		
		
		l3 = new Label();
		l3.setText("Interest Ratio");
		l3.setLayoutX(50);
		l3.setLayoutY(160);
		pane.getChildren().add(l3);
		
		t3 = new TextField();
		t3.setLayoutX(50);
		t3.setLayoutY(190);
		t3.setPrefSize(200, 20);
		pane.getChildren().add(t3);
		
		l2 = new Label();
		l2.setText("Account Number");
		l2.setLayoutX(50);
		l2.setLayoutY(50);
		pane.getChildren().add(l2);
		
		t2 = new TextField();
		t2.setLayoutX(50);
		t2.setLayoutY(80);
		t2.setPrefSize(200, 20);
		pane.getChildren().add(t2);
		
		l1 = new Label();
		l1.setText("Enter amount:");
		l1.setLayoutX(50);
		l1.setLayoutY(110);
		pane.getChildren().add(l1);
		
		t1 = new TextField();
		t1.setLayoutX(50);
		t1.setLayoutY(130);
		t1.setPrefSize(200, 20);
		pane.getChildren().add(t1);
		
		b1 = new Button();
		b1.setText("Add Savings Account");
		b1.setLayoutX(50);
		b1.setLayoutY(220);
		b1.setPrefSize(95, 20);
		pane.getChildren().add(b1);
	    
		b2 = new Button();
		b2.setText("Add Spendings Account");
		b2.setLayoutX(155);
		b2.setLayoutY(220);
		b2.setPrefSize(95, 20);
		pane.getChildren().add(b2);
	    
		b3 = new Button();
		b3.setText("Back");
		b3.setLayoutX(50);
		b3.setLayoutY(300);
		b3.setPrefSize(200, 20);
		pane.getChildren().add(b3);
	    
	}
}
