package PT2017.Homework4.View;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;

public class BankView {
	public Pane pane;
	public Scene scene;
	public Label l1;//greetings label
	public Button b1;//set up Bank
	public Button b2;//person
	public Button b3;//exit
	
	public BankView(){
		pane = new Pane();
		scene = new Scene(pane,300,400);
		
		l1 = new Label("      	   Welcome to the Bank!\n");
		l1.setLayoutX(50);
		l1.setLayoutY(50);
		pane.getChildren().add(l1);
		
		b1 = new Button();
		b1.setText("Load Bank");
		b1.setLayoutX(50);
		b1.setLayoutY(100);
		b1.setPrefSize(200, 20);
		b1.setDisable(false);
		pane.getChildren().add(b1);
		
		b2 = new Button();
		b2.setText("Manage Persons");
		b2.setLayoutX(50);
		b2.setLayoutY(200);
		b2.setPrefSize(200, 20);
		b2.setDisable(true);
		pane.getChildren().add(b2);
		
		
		b3 = new Button();
		b3.setText("Exit Application");
		b3.setLayoutX(50);
		b3.setLayoutY(300);
		b3.setPrefSize(200, 20);
		b3.setDisable(false);
		pane.getChildren().add(b3);
		
		
	}
	
}
