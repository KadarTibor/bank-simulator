package PT2017.Homework4.View;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class SetUpView {
	public Pane pane;
	public Scene scene;
	public Label l1;//create new file
	public Label l2;//select existing file
	public Label l3;//error label
	public TextField t1;//new file name;
	public Button b1;//load file
	public Button b2;//create new file
	public Button b3;//back
	
	public SetUpView(){
		pane = new Pane();
		scene = new Scene(pane,300,270);
		
		l1 = new Label("Create new File:");
		l1.setLayoutX(50);
		l1.setLayoutY(50);
		pane.getChildren().add(l1);
		
		l3 = new Label();
		l3.setLayoutX(0);
		l3.setVisible(false);
		l3.setLayoutY(250);
		pane.getChildren().add(l3);
		
		t1 = new TextField();
		t1.setPromptText("new file name");
		t1.setLayoutX(50);
		t1.setLayoutY(80);
		t1.setPrefSize(200, 20);
		pane.getChildren().add(t1);
		
		b2 = new Button();
		b2.setText("Create new file");
		b2.setLayoutX(50);
		b2.setLayoutY(110);
		b2.setPrefSize(200,20);
		pane.getChildren().add(b2);
		
		l2 = new Label("Load existing file");
		l2.setLayoutX(50);
		l2.setLayoutY(150);
		pane.getChildren().add(l2);
		
		b1 = new Button();
		b1.setText("Choose file");
		b1.setLayoutX(50);
		b1.setLayoutY(180);
		b1.setPrefSize(200,20);
		pane.getChildren().add(b1);
		
		b3 = new Button();
		b3.setText("Back");
		b3.setLayoutX(50);
		b3.setLayoutY(210);
		b3.setPrefSize(200,20);
		pane.getChildren().add(b3);
		
		
		
	}
}
