package PT2017.Homework4.View;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.plaf.synth.SynthSpinnerUI;

import PT2017.Homework4.Account.Account;
import PT2017.Homework4.Account.SavingAccount;
import PT2017.Homework4.Account.SpendingAccount;
import PT2017.Homework4.Bank.Bank;
import PT2017.Homework4.Person.Person;
import PT2017.Homework4.fileIO.ReadSequentialFile;
import PT2017.Homework4.fileIO.WriteSequentialFile;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class Controller implements EventHandler<ActionEvent> {
	public BankView bankView;
	public ManageAccountView manageAccounts;
	public PersonView personView;
	public AccountView accountView;
	public SetUpView setupView;
	public Stage stage;
	public WriteSequentialFile writer;
	public ReadSequentialFile reader;
	public Bank bank;
	public String loadingFile;
	public Person focusedPerson;
	public Account selectedAccount;
	
	
	public Controller(BankView bankView, PersonView personView,AccountView accountView,SetUpView setupView,ManageAccountView manageAccounts, Stage stage){
		this.bankView = bankView;
		this.personView = personView;
		this.accountView = accountView;
		this.setupView = setupView;
		this.stage = stage;
		this.manageAccounts = manageAccounts;
		stage.setScene(bankView.scene);
		writer = new WriteSequentialFile();
		reader = new ReadSequentialFile();
		
		
		bankView.b1.setOnAction(this);
		bankView.b3.setOnAction(this);
		bankView.b2.setOnAction(this);
		
		setupView.b3.setOnAction(this);
		setupView.b2.setOnAction(this);
		setupView.b1.setOnAction(this);
		
		personView.b1.setOnAction(this);
		personView.b2.setOnAction(this);
		personView.b3.setOnAction(this);
		personView.b4.setOnAction(this);
		personView.b5.setOnAction(this);
		personView.t2.textProperty().addListener((observable,oldValue, newValue)-> {
			 if (!newValue.matches("\\d*")) 
		        personView.t2.setText(newValue.replaceAll("[^\\d]", ""));
			 if(newValue.length()>5)
				 personView.t2.setText(oldValue);
			
		});
		personView.cb1.setOnMousePressed(new EventHandler<MouseEvent>() {
		    @Override 
		    public void handle(MouseEvent event) {
		        if (event.getClickCount() == 2) {
		        	focusedPerson = (Person) personView.cb1.getSelectionModel().getSelectedItem();
		        	personView.t1.setText(focusedPerson.getName());
		        	personView.t2.setText(focusedPerson.getPersonId()+"");
					stage.setScene(manageAccounts.scene);
					stage.centerOnScreen();
		        }
		    }
		});
		personView.tb1.setOnMousePressed(new EventHandler<MouseEvent>() {
		    @Override 
		    public void handle(MouseEvent event) {
		        if (event.getClickCount() == 2) {
		        	selectedAccount = (Account) personView.tb1.getSelectionModel().getSelectedItem();
		        	stage.setScene(accountView.scene);
		        	stage.centerOnScreen();
		        }
		    }
		});
		
		accountView.b1.setOnAction(this);
		accountView.b2.setOnAction(this);
		accountView.b3.setOnAction(this);
		accountView.t1.textProperty().addListener((observable,oldValue, newValue)-> {
			 if (!newValue.matches("\\d*")) 
			        personView.t2.setText(newValue.replaceAll("[^\\d]", ""));
			});
		
		manageAccounts.b3.setOnAction(this);
		manageAccounts.b2.setOnAction(this);
		manageAccounts.b1.setOnAction(this);
		manageAccounts.t2.textProperty().addListener((observable,oldValue, newValue)-> {
			 if (!newValue.matches("\\d*")) 
				 manageAccounts.t1.setText(newValue.replaceAll("[^\\d]", ""));
				 if(newValue.length()>5)
					 manageAccounts.t1.setText(oldValue);
				
			});
		manageAccounts.t1.textProperty().addListener((observable,oldValue, newValue)-> {
			 if (!newValue.matches("\\d*")) 
				 manageAccounts.t2.setText(newValue.replaceAll("[^\\d]", ""));	
			});
		manageAccounts.t3.textProperty().addListener((observable,oldValue, newValue)-> {
			 if (!newValue.matches("\\d.*")) 
				 manageAccounts.t3.setText(newValue.replaceAll("[^\\d]", ""));
				 if(newValue.length()>5)
					 manageAccounts.t3.setText(oldValue);
				
			});
	}
	
	
	public void handle(ActionEvent event) {
		
		if(event.getSource() == bankView.b1){
			//load bank object from file
			stage.setScene(setupView.scene);
			stage.centerOnScreen();
		}
		
		if(event.getSource() == bankView.b3){
			//exit application
			if(bank == null)
				System.exit(0);
			else{
				writer.openExistingFile(loadingFile);
				writer.writeFile(bank);
				writer.closeFile();
				System.exit(0);
			}
		}
		
		if(event.getSource() == accountView.b3){
			//get back from the account view
			stage.setScene(personView.scene);
			stage.centerOnScreen();
		}
		
		if(event.getSource() == accountView.b1){
			
			//deposti some cash
			accountView.l2.setText("");
			bank.depositMoney(selectedAccount, Integer.parseInt(accountView.t1.getText()));
			personView.tb1.refresh();
			accountView.t1.setText("");
		
		}
		
		if(event.getSource() == accountView.b2){
			//withdraw some money
			accountView.l2.setText("");
			boolean drawed = bank.withdraw(selectedAccount, Integer.parseInt(accountView.t1.getText()));
			if(drawed){
				personView.tb1.refresh();
				accountView.t1.setText("");
			}
			else{
				accountView.l2.setText("Try again in a minute/ insufficient funds");
			}
		}
		if(event.getSource() == bankView.b2){
			//manage persons
			stage.setScene(personView.scene);
			createComboBox(personView.cb1,bank.getPersons());
			stage.centerOnScreen();
			
		}
		
		if(event.getSource() == setupView.b3){
			//back from setup page
			stage.setScene(bankView.scene);
			stage.centerOnScreen();
			if(bank!=null){
				bankView.b2.setDisable(false);
			}
		}
		if(event.getSource() == setupView.b2){
			//create new file button has been pressed
			String fileName = setupView.t1.getText();
			if(!fileName.equals("")){
				loadingFile = "src/"+fileName+".sbo";
				bank = new Bank();
				setupView.t1.setText("");
				setupView.l3.setText("File was Created Successfully!");
				setupView.l3.setVisible(true);
				
			}
			else{
				setupView.l3.setText("File name must not be empty!");
				setupView.l3.setVisible(true);
			}
		}
		
		if(event.getSource() == setupView.b1){
			//chose a file from the computer
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Open Resource File");
			File initialDirectory = new File("D:\\PT2017_30423_Kadar_Tibor_Assignment_4\\Homework4\\src");
			fileChooser.setInitialDirectory(initialDirectory);
			File file = fileChooser.showOpenDialog(stage);
			loadingFile = file.toString();
			reader.openFile(file.toString());
			try{
				bank = reader.readBank();
				reader.closeFile();
				setupView.l3.setText("File was selected Successfully!");
				setupView.l3.setVisible(true);
			}
			catch(Exception e){
				setupView.l3.setText("File must end in .sbo and should contain a Bank object!");
				setupView.l3.setVisible(true);
			}
		}
		
		if(event.getSource() == personView.b5){
			stage.setScene(bankView.scene);
			stage.centerOnScreen();
			personView.l3.setText("");
		}
		
		if(event.getSource() == personView.b4){
			//show log
			personView.l3.setText("");
			Person person =(Person) personView.cb1.getSelectionModel().getSelectedItem();
			personView.ta1.setText(person.getLog());
		}
		
		if(event.getSource() == personView.b3){
			personView.l3.setText("");
			Person person = (Person) personView.cb1.getSelectionModel().getSelectedItem();
			createTable(personView.tb1,bank.getAccounts(person));
		}
		
		if(event.getSource() == personView.b2){
			//delete person
			personView.l3.setText("");
			String newPerson = personView.t1.getText();
			String newPersonId = personView.t2.getText();
			if(!newPerson.equals("")){
				if((!newPersonId.equals(""))&&( 9999 < Integer.parseInt(newPersonId) && Integer.parseInt(newPersonId) < 100000)){
					try {
						Person person = new Person(Integer.parseInt(newPersonId),newPerson);
						bank.removePerson(person);
						personView.t1.setText("");
						personView.t2.setText("");
						personView.l3.setText("Person " + newPerson +" was deleted from the system!");
						createComboBox(personView.cb1,bank.getPersons());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else{
					//person id must be greater than 9999 
					personView.l3.setText("Id must be greater than 9999!");
				}
			}
			else{
				//name cannot be empty
				personView.l3.setText("A person must have a name!");
			}
		}
		
		if(event.getSource() == personView.b1){
			//add person
			personView.l3.setText("");
			String newPerson = personView.t1.getText();
			String newPersonId = personView.t2.getText();
			if(!newPerson.equals("")){
				if((!newPersonId.equals(""))&&( 9999 < Integer.parseInt(newPersonId) && Integer.parseInt(newPersonId) < 100000)){
					try {
						bank.addPerson(new Person(Integer.parseInt(newPersonId),newPerson));
						personView.t1.setText("");
						personView.t2.setText("");
						personView.l3.setText("Person " + newPerson +" was introduced in the system!");
						createComboBox(personView.cb1,bank.getPersons());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else{
					//person id must be greater than 9999 
					personView.l3.setText("Id must be greater than 9999!");
				}
			}
			else{
				//name cannot be empty
				personView.l3.setText("A person must have a name!");
			}
		}
		if(event.getSource() == manageAccounts.b3){
			//get back from the account management
			stage.setScene(personView.scene);
			stage.centerOnScreen();
		}
		
		if(event.getSource() == manageAccounts.b2){
			//create spendings account
			try {
				SpendingAccount account = new SpendingAccount(new Date(),Integer.parseInt(manageAccounts.t2.getText()),Integer.parseInt(manageAccounts.t1.getText()),focusedPerson);
				bank.addAccount(focusedPerson, account);
				manageAccounts.t1.setText("");
				manageAccounts.t2.setText("");
				manageAccounts.t3.setText("");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(event.getSource() == manageAccounts.b1){
			//create savings account
			try {
				SavingAccount account = new SavingAccount(new Date(),Integer.parseInt(manageAccounts.t2.getText()),Integer.parseInt(manageAccounts.t1.getText()),Float.parseFloat(manageAccounts.t3.getText()),focusedPerson);
				bank.addAccount(focusedPerson, account);
				manageAccounts.t1.setText("");
				manageAccounts.t2.setText("");
				manageAccounts.t3.setText("");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		
		
	}
	
	   @SuppressWarnings({ "unchecked", "rawtypes" })
		private void createTable(TableView table,List<? extends Account> objects){
		   
			   ObservableList<? extends Account> data = FXCollections.observableArrayList(objects);
			   table.setItems(data);	
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		private void createComboBox(ComboBox comboBox,List<Person> objects){
	    	ObservableList<Person> data = FXCollections.observableArrayList(objects);
	    	comboBox.setItems(data);
	    	comboBox.getSelectionModel().selectFirst();
	    }
	    
	   
	
	
}
