package PT2017.Homework4.View;


import PT2017.Homework4.Account.Account;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

public class PersonView {
	public Pane pane;
	public Scene scene;
	public Label l1;//name
	public Label l2;//identification number
	public Label l3;//label for errors
	public TextField t1;//name
	public TextField t2;//identification number
	public Button b1;//add person
	public Button b2;//delete person
	public Button b3;//display accounts and informations regarding the accounts
	public Button b4;//show log
	public Button b5;//back
	public TextArea ta1;//log
	public ComboBox cb1;//persons
	public TableView<Account> tb1;//accounts
	
	
	public PersonView(){
		pane = new Pane();
		scene = new Scene(pane,1150,400);
		
		tb1 = new TableView<Account>();
		tb1.setLayoutX(750);
		tb1.setLayoutY(80);
		tb1.setPrefSize(350, 200);
		
		TableColumn<Account,Integer> accountNumber = new TableColumn<Account,Integer>("Acc. Nr.");
		TableColumn<Account,Double> balance = new TableColumn<Account,Double>("Balance");
		accountNumber.setCellValueFactory( new PropertyValueFactory<Account,Integer>("accountNumber"));
        balance.setCellValueFactory(new PropertyValueFactory<Account,Double>("balance"));
		
        accountNumber.setMinWidth(175);
        balance.setMinWidth(175);
        tb1.getColumns().addAll(accountNumber, balance);  
		pane.getChildren().add(tb1);
		
		ta1 = new TextArea();
		ta1.setLayoutX(300);
		ta1.setLayoutY(130);
		ta1.setPrefSize(400, 150);
		//ta1.setVisible(false);
		pane.getChildren().add(ta1);
		
		cb1 = new ComboBox();
		cb1.setLayoutX(300);
		cb1.setLayoutY(80);
		cb1.setPrefSize(400, 20);
		//cb1.setVisible(false);
		pane.getChildren().add(cb1);
		
		l1 = new Label();
		l1.setText("Person Name:");
		l1.setLayoutX(50);
		l1.setLayoutY(60);
		pane.getChildren().add(l1);
		

		t1 = new TextField();
		t1.setPromptText("Tache Farfuridi");
		t1.setPrefSize(200, 20);
		t1.setLayoutX(50);
		t1.setLayoutY(80);
		pane.getChildren().add(t1);
		
		
		l2 = new Label();
		l2.setText("Identification Number:");
		l2.setLayoutX(50);
		l2.setLayoutY(110);
		pane.getChildren().add(l2);
		
		l3 = new Label();
		l3.setText("");
		l3.setLayoutX(50);
		l3.setLayoutY(320);
		pane.getChildren().add(l3);
		
		
		
		t2 = new TextField();
		t2.setPromptText("-----");
		t2.setPrefSize(200, 20);
		t2.setLayoutX(50);
		t2.setLayoutY(130);
		pane.getChildren().add(t2);
		
		b1 = new Button();
		b1.setText("Add Person");
		b1.setLayoutX(50);
		b1.setLayoutY(160);
		b1.setPrefSize(200,	20);
		pane.getChildren().add(b1);		
		
		b2 = new Button();
		b2.setText("Delete Person");
		b2.setLayoutX(50);
		b2.setLayoutY(190);
		b2.setPrefSize(200,	20);
		pane.getChildren().add(b2);		
		
		b3 = new Button();
		b3.setText("Show Accounts");
		b3.setLayoutX(50);
		b3.setLayoutY(220);
		b3.setPrefSize(200,	20);
		pane.getChildren().add(b3);		
		
		b4 = new Button();
		b4.setText("Show log");
		b4.setLayoutX(50);
		b4.setLayoutY(250);
		b4.setPrefSize(200,	20);
		pane.getChildren().add(b4);		
		
		b5 = new Button();
		b5.setText("Back");
		b5.setLayoutX(50);
		b5.setLayoutY(350);
		b5.setPrefSize(200,	20);
		pane.getChildren().add(b5);		
		
		
	}
}
