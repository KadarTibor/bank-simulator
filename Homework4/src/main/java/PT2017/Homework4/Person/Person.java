package PT2017.Homework4.Person;

import java.io.Serializable;

public class Person implements Serializable,Observer{

	   
	
	private static final long serialVersionUID = -8595737165652445822L;
		private int personId;
		private String name;
		private StringBuilder log;
		
		public Person(int id, String name) throws Exception{
			if(id > 9999 && id < 100000)
					this.personId = id;
			else
				throw new Exception("Person id doesnt fit!");
			this.name = name;
			this.log = new StringBuilder("");
		}
		
		public int getPersonId() {
			return personId;
		}


		public void setPersonId(int personId) {
			this.personId = personId;
		}


		public String getName() {
			return name;
		}


		public void setName(String name) {
			this.name = name;
		}
		
		@Override
		public int hashCode(){
			return personId;
		}
		
		@Override
	    public boolean equals(Object o) {

	        if (o == this) return true;
	        if (!(o instanceof Person)) {
	            return false;
	        }

	        Person person = (Person) o;

	        return person.name.equals(name) && person.personId == personId;
	    }
		
		@Override
		public String toString(){
			return "Person " + name + " id " + personId;
		}

		public void update(String update) {
			log.append("\n" + update);
		}
		
		public String getLog(){
			
			return log.toString();
		}
}
