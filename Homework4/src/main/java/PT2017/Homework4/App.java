package PT2017.Homework4;

import java.io.File;
import java.util.Date;

import PT2017.Homework4.Account.SavingAccount;
import PT2017.Homework4.Bank.Bank;
import PT2017.Homework4.Person.Person;
import PT2017.Homework4.View.AccountView;
import PT2017.Homework4.View.BankView;
import PT2017.Homework4.View.Controller;
import PT2017.Homework4.View.ManageAccountView;
import PT2017.Homework4.View.PersonView;
import PT2017.Homework4.View.SetUpView;
import PT2017.Homework4.fileIO.ReadSequentialFile;
import PT2017.Homework4.fileIO.WriteSequentialFile;
import javafx.application.Application;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class App extends Application
{
    public static void main( String[] args )
    {
    	launch();
    	/*
      try{
    	Bank bank = new Bank();
        Person a = new Person(1000,"Misi");
        System.out.println(a);
        Person b = new Person(1001,"Susi");
        System.out.println(b);
        bank.addPerson(a);
        bank.addPerson(b);
        Date azi = new Date();
        //System.out.println("here" + azi);
        SavingAccount c = new SavingAccount(azi,10000,10.0,0.5f,a);
        
        System.out.println(c);
        bank.addAccount(a, c);
        
        c.deposit(new Date(), 100);
       // Account d = bank.
        System.out.println(a.getLog());
        
        WriteSequentialFile outfile = new WriteSequentialFile();
        outfile.openNewFile("src/bank.sbo");
        outfile.writeFile(bank);
        outfile.closeFile();
        ReadSequentialFile infile = new ReadSequentialFile();
        infile.openFile("src/bank.sbo");
        Bank bank1 = infile.readBank();
        infile.closeFile();
        
        System.out.println("Misi has money " + bank1.getMoneyOfPerson(a));
      }
      catch(Exception e){
    	  e.printStackTrace();
      }
      */
    }
    
   
	@Override
	public void start(Stage primaryStage) throws Exception {
		
			primaryStage.setTitle("Bank Application");
			primaryStage.setResizable(false);
			primaryStage.initStyle(StageStyle.UNDECORATED);
			ManageAccountView manageAccounts = new ManageAccountView();
			BankView bankView = new BankView();
			SetUpView setupView = new SetUpView();
			PersonView personView = new PersonView();
			AccountView accountView = new AccountView();
			Controller controller = new Controller(bankView,personView,accountView,setupView,manageAccounts,primaryStage);
			//primaryStage.setScene(accountView.scene);
			primaryStage.show();
			
	}
}
