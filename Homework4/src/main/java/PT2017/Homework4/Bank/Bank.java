package PT2017.Homework4.Bank;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import PT2017.Homework4.Account.Account;
import PT2017.Homework4.Person.Person;

public class Bank implements BankProc,Serializable {

	private static final long serialVersionUID = -7024437217208501731L;
	private List<Person> persons; 
	private HashMap<Person,List<Account>> map;
	
	public Bank(){
		this.persons = new ArrayList<Person>();
		this.map = new HashMap<Person,List<Account>>();
	}
	
	public void addPerson(Person person) {
		
		assert (!persons.contains(person));
		persons.add(person);
		map.put(person, new ArrayList());
		assert(persons.contains(person));
		assert isWellFormed();
	}

	public void removePerson(Person person) {
		assert (persons.contains(person));
		
		persons.remove(person);
		map.remove(person);
		
		assert(!persons.contains(person));
		assert isWellFormed();
		
	}
	public List<Account> getAccounts(Person holder){
		return map.get(holder);
	}
	public void addAccount(Person holder, Account account) {
		
		assert(persons.contains(holder));	
		List<Account> accounts =map.get(holder);
		if(!accounts.contains(account)){
			accounts.add(account);
			map.put(holder,accounts);
		}
		
		assert(map.get(holder).contains(account));
		assert isWellFormed();
		
	}

	public void removeAccount(Person holder, Account account) {

		assert(persons.contains(holder));
		
		assert(map.get(holder).contains(account));
		
		map.remove(holder, account);
		
		assert(!map.get(holder).contains(account));
		assert isWellFormed();
	}
	

	public String readAccount(Person holder,Account account) {
		
			assert(persons.contains(holder));
		
			assert(map.get(holder).contains(account));
			
			assert isWellFormed();
			return account.toString();
		
		
	}

	
	//get the list of persons
	public List<Person> getPersons(){
		return persons;
	}
	
	//get the money of a person
	public double getMoneyOfPerson(Person holder){
		double sum = 0;
		for(Account account: map.get(holder)){
			sum += account.getBalance();
		}
		assert isWellFormed();
		return sum;
		
	}
	
	public void depositMoney(Account account, double sum){
		account.deposit(sum, new Date());
	}
	
	public boolean withdraw(Account account, double sum){
		return account.withdraw(sum, new Date());
	}
	//checks whether the persons are relevand and have entries in the hashTable
	public boolean isWellFormed(){
		for(Person person:persons){
			if(map.get(person)==null)
				return false;
		}
		return true;
	}

}
