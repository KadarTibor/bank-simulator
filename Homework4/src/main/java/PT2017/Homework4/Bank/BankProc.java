package PT2017.Homework4.Bank;

import PT2017.Homework4.Account.Account;
import PT2017.Homework4.Person.Person;

public interface BankProc {
	/**
	 *  Adds a Person into the bank system
	 * @pre person does not exist in the list of persons
	 * @post person exists in the list of persons
	 */
	public void addPerson(Person person);
	/**
	 *  Removes a Person into the bank system
	 * @pre person exists in the list of persons
	 * @post person does not exist in the list of persons
	 */
	public void removePerson(Person person);
	
	/**
	 *  Adds an Account into the list of accounts of the holder
	 * @pre person exists account is not already associated with the holder
	 * @post account is associatedwith 
	 */
	public void addAccount(Person holder, Account account);
	
	/**
	 *  Removes an Account from the list of accounts of the holder
	 * @pre account is  associated with the holder
	 * @post account is no longer  associated with the holder
	 */
	public void removeAccount(Person holder, Account account);
	
	/**
	 * Will inspect and display the state of the account
	 * @pre account exists
	 * @post no post conditions
	 */
	public String readAccount(Person holder, Account account);
	
	/**
	 * Will generate reports based on the state of the bank system
	 * -total amount of money
	 * -largest deposit
	 * -smallest deposit
	 * -list of all the accounts
	 * -list of all the persons
	 *  
	 * @pre persons list is not empty
	 */
	
	/**
	 * @pre all persons are valid and different from null
	 * @post no change
	 */
	public boolean isWellFormed();
}
