package PT2017.Homework4.Account;



import java.util.Date;

import PT2017.Homework4.Person.Person;


public class SavingAccount extends Account implements Observable{

	private static final long serialVersionUID = 70931372054259352L;
	private Person holder;
	private float interestRatio;
	private Date lastTransaction;
	
	public SavingAccount(Date createdTime, int accountNumber, double balance,float interestRatio,Person holder) throws Exception {
		
		super(createdTime, accountNumber, balance);		
		attach(holder);		
		notifyObserver("Savings Account with nr: " + getAccountNumber() + " was created and the sum " + getBalance()+ " was deposited.");
		this.interestRatio = interestRatio;
		this.lastTransaction = createdTime;
		
	}
	
	public void deposit(double newSum,Date currentTime){
		addInterest(currentTime);
		setBalance(getBalance() + newSum);
		lastTransaction = currentTime;
		notifyObserver("Deposit of " + newSum + " to the Spending account with nr: " + getAccountNumber() + " at " + lastTransaction.toString());
	}
	
	public boolean withdraw(double sum, Date currentTime){
		
		if(currentTime.getTime() >= lastTransaction.getTime() + 60000){
			if(sum < getBalance()){	
				addInterest(currentTime);
				setBalance(getBalance() - sum);
				lastTransaction = currentTime;
				notifyObserver("Withdrawal of " + sum + " from the Savings account with nr: " + getAccountNumber() + " at " + lastTransaction.toString());
				return true;
			}
			else{
				notifyObserver("Withdrawal attempt of " + sum + " from the Spending account with nr: " + getAccountNumber() + " at " + currentTime.toString());
				return false;
			}
		}
		return false;
		
	}
	
	public double calculateInterest(Date time){
		return getBalance()*interestRatio*(time.getTime() - lastTransaction.getTime())/(3.1556926 * Math.pow(10,10));
	}
	
	private void addInterest(Date currentTime){
		setBalance(getBalance() + calculateInterest(currentTime));
		notifyObserver("Interest has been added to the account nr: " + getAccountNumber() +" at " + currentTime.toString());
	}
	
	public float getInterestRatio() {
		return interestRatio;
	}

	public void setInterestRatio(float interestRatio) {
		this.interestRatio = interestRatio;
		notifyObserver("Interest ratio has been set for the account nr: " + getAccountNumber());
	}
	
	public Date getLastTransaction() {
		return lastTransaction;
	}

	public void setLastTransaction(Date lastTransaction) {
		this.lastTransaction = lastTransaction;
	}
	
	@Override
	public String toString(){
		return "Savings Account " + getAccountNumber() + " balance: " + getBalance(); 
	}

	public void attach(Person holder) {
		this.holder = holder;
	}

	public void detach() {
		this.holder = null;	
	}

	public void notifyObserver(String update) {
		
		holder.update(update);
		
	}
}
