package PT2017.Homework4.Account;

import PT2017.Homework4.Person.Person;

public interface Observable {
	public void attach(Person holder);
	public void detach();
	public void notifyObserver(String update);
}
