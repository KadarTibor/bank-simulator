package PT2017.Homework4.Account;

import java.util.Date;

import PT2017.Homework4.Person.Person;


public class SpendingAccount extends Account implements Observable {
	

	
	private static final long serialVersionUID = 6508544150134952810L;
	private Person holder;
	private Date lastTransaction;
	
	public SpendingAccount(Date createdTime, int accountNumber, double balance,Person holder) throws Exception {
		super(createdTime, accountNumber, balance);
		this.lastTransaction = createdTime;
		attach(holder);
		notifyObserver("Spendings Account with nr: " + getAccountNumber() + " was created and the sum " + getBalance()+ " was deposited.");
	}
	
	public boolean withdraw(double newSum,Date currentTime){
		if(currentTime.getTime()>lastTransaction.getTime()){
			if(newSum < getBalance()){
				setBalance(getBalance() - newSum);
				lastTransaction = currentTime;
				notifyObserver("Withdrawal of " + newSum + " from the Spending account with nr: " + getAccountNumber() + " at " + lastTransaction.toString());
				return true;
			}
			else{
				notifyObserver("Withdrawal attempt of " + newSum + " from the Spending account with nr: " + getAccountNumber() + " at " + currentTime.toString());
				return false;
				
			}
		}
		else
			return false;
	}
	
	public void deposit(double newSum, Date currentTime){
		if(currentTime.getTime() > lastTransaction.getTime()){
			setBalance(getBalance() + newSum );
			lastTransaction = currentTime;
			notifyObserver("Deposit of " + newSum + " to the Spending account with nr: " + getAccountNumber() + " at " + lastTransaction.toString());
		}
	}
	
	@Override
	public String toString(){
		return "Spendings Account " + getAccountNumber() + " balance: " + getBalance(); 
	}

	public void attach(Person holder) {
		this.holder  = holder;
	}

	public void detach() {
		this.holder = null;
		
	}
	public void notifyObserver(String update) {
		holder.update(update);
	}

	
}
