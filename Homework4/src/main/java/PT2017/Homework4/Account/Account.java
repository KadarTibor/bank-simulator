package PT2017.Homework4.Account;

import java.io.Serializable;
import java.util.Date;


public abstract class Account implements Serializable {

	private static final long serialVersionUID = -6625403376048289857L;
	private int accountNumber;
	private double balance;
	@SuppressWarnings("unused")
	private Date createdTime;
	
	public Account(Date createdTime,int accountNumber, double balance) throws Exception{
		this.createdTime = createdTime;
		System.out.println(accountNumber);
		if(accountNumber > 9999 && accountNumber < 100000){
			
			
			this.accountNumber = accountNumber;
		}
		else
			throw new Exception("Account number does not comply the constraints!");
		this.balance = balance;
	}
	
	public int getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public abstract void deposit(double newSum,Date currentTime);
	public abstract boolean withdraw(double sum,Date currentTime);
	
	
}
